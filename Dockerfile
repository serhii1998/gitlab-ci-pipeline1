FROM ubuntu:18.04
# Install necessary packages
RUN apt-get -y update && apt-get -y install zip curl openjdk-8-jre-headless maven nginx && rm -rf /var/lib/apt/lists/*
# for Centos OS
#RUN yum update -y && yum install -y epel-release && yum install -y git curl dpkg java java-devel unzip 

ARG user=jenkins
ARG JENKINS_HOME=/var/lib/jenkins
ARG REF=/usr/share/jenkins/ref

ENV JENKINS_HOME $JENKINS_HOME
ENV REF $REF
# Variables for script default-user.groovy and for access to Jenkins Dashboard
ENV JENKINS_USER admin
ENV JENKINS_PASS admin
# Jenkins is run with user `jenkins`
RUN mkdir -p $JENKINS_HOME \
  && chown 1000:1000 $JENKINS_HOME \
  && groupadd -g 1000 jenkins \
  && useradd -d "$JENKINS_HOME" -u 1000 -g 1000 -m -s /bin/bash ${user} \
  && mkdir -p ${REF}\
  && chown -R ${user} "$JENKINS_HOME" "$REF" 

# Download Jenkins
RUN curl -fsSL http://mirrors.jenkins.io/war-stable/latest/jenkins.war -o /usr/share/jenkins/jenkins.war --create-dirs

ENV JENKINS_UC=https://updates.jenkins.io
ENV JENKINS_UC_EXPERIMENTAL=https://updates.jenkins.io/experimental
ENV JENKINS_INCREMENTALS_REPO_MIRROR=https://repo.jenkins-ci.org/incrementals

# Open port 80 for access

EXPOSE 80


# Change user to jenkins
#USER ${user}
# Copy local config files to image

COPY /config/nginx.conf /etc/nginx/sites-available/default
COPY start.sh /usr/local/bin/start.sh
COPY /config/default-user.groovy ${JENKINS_HOME}/init.groovy.d/default-user.groovy
COPY /config/plugins.txt /usr/share/jenkins/ref/plugins.txt
COPY /config/jenkins-support /usr/local/bin/jenkins-support
COPY /config/install-plugins.sh /usr/local/bin/install-plugins.sh

# install necessary plugins from plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt && ls -la ${JENKINS_HOME} 

#RUN mkdir -p ${JENKINS_HOME}/plugins &&  cp ${REF}/plugins/*.jpi ${JENKINS_HOME}/plugins \
#   && echo "List of plugins in folder ${JENKINS_HOME}/plugins" && ls -la ${JENKINS_HOME}/plugins

# Run jenkins.war with skipping Setup Wizard
ENTRYPOINT ["start.sh"]
#CMD ["-Djenkins.install.runSetupWizard=false","-jar","/usr/share/jenkins/jenkins.war"]

VOLUME $JENKINS_HOME
